import 'package:flutter/material.dart';
import 'package:flutter_free_map/pages/map_bloc.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final LatLng _initialLatLng = LatLng(-7.0845390, -41.465149);
  MapBloc _controller;
  @override
  void initState() {
    super.initState();
    _controller = new MapBloc(_initialLatLng);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('Map Application'),
      ),
      body: Stack(
        children: <Widget>[
          StreamBuilder<LatLng>(
            initialData: _initialLatLng,
            stream: _controller.stream,
            builder: (context, snapshot) {
              return new FlutterMap(
                options: new MapOptions(
                  onTap: (latlong) {
                    _controller.add(latlong);
                  },
                  center: _initialLatLng,
                ),
                layers: [
                  new TileLayerOptions(
                      urlTemplate:
                          'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                      subdomains: ['a', 'b', 'c']),
                  new MarkerLayerOptions(
                    markers: [
                      Marker(
                        width: 45.0,
                        height: 45.0,
                        point: snapshot.data,
                        builder: (context) => new Container(
                          child: IconButton(
                            icon: Icon(
                              Icons.person_pin_circle,
                              color: Colors.red[700],
                            ),
                            onPressed: () {
                              print('Marker tapped!');
                            },
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              );
            }
          )
        ],
      ),
    );
  }
}
