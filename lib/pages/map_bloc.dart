import 'package:flutter_free_map/utils/generic_bloc.dart';
import 'package:latlong/latlong.dart';

class MapBloc extends GenericBloc<LatLng> {
  final LatLng _latLong;
  MapBloc(this._latLong){
    add(this._latLong);
  }
}
